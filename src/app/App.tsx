import React from 'react'
import { createRoot } from 'react-dom/client'
import { Router } from 'router'
import './styles.css'
import { axiosConfig } from 'config'

const container = document.getElementById('root')!
export const root = createRoot(container)

axiosConfig()

root.render(
  <React.StrictMode>
    <Router />
  </React.StrictMode>
)
