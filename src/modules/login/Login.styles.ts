import { styled, TextField as BaseTextField, Button as BaseButton } from "@mui/material";



export const TextField = styled(BaseTextField)({
    display: "flex",
    flexDirection: "column",
    gap: "10px",
    color: 'white',
    backgroundColor: '#999999',
    maxWidth: '400px',
    width: '70vw'
})
export const Button = styled(BaseButton)({
    flex: 1
})

export const Container = styled("div")({
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    gap: "10px"
    
})