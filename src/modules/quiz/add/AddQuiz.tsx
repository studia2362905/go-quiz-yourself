import React, { useState } from 'react'
import { useNavigate } from 'react-router-dom'
import {
  Container,
  Name,
  Question,
  QuizName,
  Button,
  ButtonContainer,
  Choice,
  ButtonGroup,
  ToggleButton,
  Box,
} from './AddQuiz.styles'
import axios from 'axios'
enum state {
  AddName,
  AddQuestion,
}

const AddQuiz = () => {
  const navigate = useNavigate()
  const emptyChoiceList = [
    { text: '' },
    { text: '' },
    { text: '' },
    { text: '' },
  ]
  const [newQuizName, setNewQuizName] = useState<string>('')
  const [newQuestion, setNewQuestion] = useState<string>('')
  const [newChoice, setNewChoice] =
    useState<{ text: string; is_correct?: boolean }[]>(emptyChoiceList)
  const [pageState, setPageState] = useState<state>(state.AddName)
  const [correctChoice, setCorrectChoice] = useState<number>(0)
  const [quizId, setQuizId] = useState<number>(0)

  const AddName = () => {
    const handleAddName = () => {
      axios
        .post('/quiz', { name: newQuizName })
        .then((res) => res.data)
        .then((res) => setQuizId(res.id))
        .then(() => setPageState(state.AddQuestion))
    }
    return (
      <Box>
        <QuizName
          label='QuizName'
          variant='filled'
          value={newQuizName}
          onChange={(e) => setNewQuizName(e.target.value)}
        />
        <Button sx={{maxWidth:'400px'}}variant='contained' disabled={newQuizName === ''} onClick={() => handleAddName()}>
          Add Question
        </Button>
      </Box>
    )
  }
  const AddQuestion = () => {
    const handleAddQuestion = async () => {
      let data = [...newChoice]
      data[correctChoice] = { ...data[correctChoice], is_correct: true }
      console.log(data)
      axios
        .post(`/quiz/${quizId}`, { text: newQuestion, choices: data })
        .then((res) => res.data)
        .then((res) => console.log(res))
        .then(() => {
          setNewChoice(emptyChoiceList)
          setNewQuestion('')
          setCorrectChoice(0)
        })
    }
    const handleFinishQuiz = () => {
      navigate(`/`)
    }
    return (
      <Box>
        <Question
          label='NewQuestion'
          variant='filled'
          value={newQuestion}
          onChange={(e) => setNewQuestion(e.target.value)}
        />
        <Choice
          label='Answer 1'
          variant='filled'
          value={newChoice[0].text}
          onChange={(e) => {
            let arr = [...newChoice]
            arr[0] = { ...arr[0], text: e.target.value }
            setNewChoice(arr)
          }}
        />
        <Choice
          label='Answer 2'
          variant='filled'
          value={newChoice[1].text}
          onChange={(e) => {
            let arr = [...newChoice]
            arr[1] = { ...arr[1], text: e.target.value }
            setNewChoice(arr)
          }}
        />
        <Choice
          label='Answer 3'
          variant='filled'
          value={newChoice[2].text}
          onChange={(e) => {
            let arr = [...newChoice]
            arr[2] = { ...arr[2], text: e.target.value }
            setNewChoice(arr)
          }}
        />
        <Choice
          label='Answer 4'
          variant='filled'
          value={newChoice[3].text}
          onChange={(e) => {
            let arr = [...newChoice]
            arr[3] = { ...arr[3], text: e.target.value }
            setNewChoice(arr)
          }}
        />
        <ButtonGroup
          exclusive
          value={correctChoice}
          onChange={(_, newstate) => setCorrectChoice(newstate)}
        >
          <ToggleButton value={0}>Ans1</ToggleButton>
          <ToggleButton value={1}>Ans2</ToggleButton>
          <ToggleButton value={2}>Ans3</ToggleButton>
          <ToggleButton value={3}>Ans4</ToggleButton>
        </ButtonGroup>
        <ButtonContainer>
          <Button variant='contained' onClick={() => handleFinishQuiz()}>Finish</Button>
          <Button variant='contained' onClick={() => handleAddQuestion()}>AddQuestion</Button>
        </ButtonContainer>
      </Box>
    )
  }

  return (
    <>
      <Container>
        <Name>Create Brand New Quiz</Name>
        {pageState === state.AddQuestion ? AddQuestion() : AddName()}
      </Container>
    </>
  )
}

export { AddQuiz }
