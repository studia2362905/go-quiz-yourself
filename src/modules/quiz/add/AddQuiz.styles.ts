import { TextField, styled ,ToggleButton as BaseToggleButton, Button as BaseButton, ToggleButtonGroup as BaseToggleButtonGroup} from "@mui/material"
  
export const Container = styled("div")({
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    gap: '10px',
    minHeight: '80vh'
})
export const Box = styled('div')({
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    flex: '1',
    gap: '10px',
    width: '700px',
    maxWidth: '70vw'
})

export const Name = styled("div")({
    gap: "10px",
    backgroundColor: '#222222',
    fontSize: "60px", 
    color: 'white',
    width: "40%",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
})

export const QuizName = styled(TextField)({
    display: "flex",
    flexDirection: "column",
    gap: "10px",
    color: 'white',
    backgroundColor: '#999999',
    maxWidth: '400px',
    width: '70vw'
})

export const Question = styled(TextField)({
    display: "flex",
    flexDirection: "column",
    gap: "10px",
    width: '100%',
    backgroundColor: '#999999',
})

export const Choice = styled(TextField)({
    backgroundColor: '#999999',
    width: '100%',
})

export const Button = styled(BaseButton)({
width: '100%',

})
export const ButtonContainer = styled("div")({
    display: "flex",
    flexDirection: "row",
    width: "100%",
    gap: '10px',
})
export const ToggleButton = styled(BaseToggleButton)({
    backgroundColor: '#999999',
    "&.Mui-selected, &.Mui-selected:hover": {
        color: "white",
        backgroundColor: '#1976d2',
      }
})
export const ButtonGroup = styled(BaseToggleButtonGroup)({
    display: "flex",
    flexDirection: "row",
    width: '100%',
    justifyContent: 'center'
})