import { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'
import { useNavigate } from 'react-router-dom'
import {
  Container,
  Text,
  EndQuizButton,
  Name,
  NextQuestionButton,
  Question,
  Choice,
  Box,
} from './Quiz.styles'
import axios from 'axios'
import {
  Choice as ChoiceType,
  Question as QuestionType,
  Quiz as QuizType,
} from 'typings'

const Quiz = () => {
  const { quizID }: { quizID?: string } = useParams()

  const [questionNumber, setQuestionNumber] = useState(0)
  const [quiz, setQuiz] = useState<QuizType | null>(null)
  const [question, setQuestion] = useState<QuestionType | null>(null)
  const [chosenAnswer, setChosenAnswer] = useState<ChoiceType>()
  const [chosenAnswers, setChosenAnswers] = useState<
    { question_id: number; answer_id: number }[]
  >([])

  const navigate = useNavigate()

  useEffect(() => {
    axios
      .get(`/quiz/${quizID}`)
      .then(function (response: { data: QuizType }) {
        setQuiz(response.data)
        setQuestion(response.data.questions[0])
      })
      .catch(function (error) {
        console.log(error)
      })
  }, [])

  useEffect(() => {
    if (quiz) {
      setQuestion(quiz.questions[questionNumber])
      chosenAnswer &&
        setChosenAnswers([
          ...chosenAnswers,
          {
            question_id: chosenAnswer.question_id,
            answer_id: chosenAnswer.id,
          },
        ])
    }
    //eslint-disable-next-line
  }, [questionNumber])

  const SingleQuestion = () => {
    return (
      <Question>
        <Text>Question {questionNumber+1}: {question?.text}</Text><Box>
        {question?.choices.map((choice: ChoiceType) => (
          <Choice
            key={choice.id}
            onClick={() => setChosenAnswer(choice)}
            sx={{
              background: choice.id === chosenAnswer?.id ? 'green' : '#999999',
            }}
          >
            {choice.text}
          </Choice>
        ))}
      </Box></Question>
    )
  }

  const handleEndQuiz = () => {
    let arr = chosenAnswers
    if (chosenAnswer) {
      arr = [
        ...chosenAnswers,
        {
          question_id: chosenAnswer.question_id,
          answer_id: chosenAnswer.id,
        },
      ]
    }
    const token = localStorage.getItem('access_token')
    const auth = {
      ...(token && { headers: { Authorization: `Bearer ${token}` } }),
    }
    axios
      .post(`/quiz/check/${quizID}`, arr, auth)
      .then((res) => res.data)
      .then((res) => navigate(`/quiz/quizend`, { state: { points: res.score, max: res.max } }))
  }

  return (
    <Container>
      {quiz ? (
        <>
          <Name>Chosen Quiz: {quiz.name}</Name><Box>
          {quiz.questions.length > 0 ? (
            <SingleQuestion />
          ) : (
            <Text>No questions available.</Text>
          )}
          {questionNumber < quiz.questions.length - 1 ? (
            <NextQuestionButton
              onClick={() => setQuestionNumber(questionNumber + 1)}
            >
              Next Question
            </NextQuestionButton>
          ) : (
            <EndQuizButton onClick={() => handleEndQuiz()}>
              Finish Quiz
            </EndQuizButton>
          )}
        </Box></>
      ) : (
        'wait'
      )}
    </Container>
  )
}

export { Quiz }
