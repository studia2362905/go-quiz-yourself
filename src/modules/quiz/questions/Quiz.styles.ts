import { Button, styled } from '@mui/material'

export const Container = styled('div')({
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'center',
  alignItems: 'center',
  gap: '10px',
  minHeight: '80vh',
})

export const Box = styled('div')({
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'center',
  alignItems: 'center',
  flex: '1',
  gap: '10px',
  maxWidth: '70vw',
})

export const Question = styled('div')({
  display: 'flex',
  flexDirection: 'column',
  gap: '100px',
  justifyContent: 'center',
  alignItems: 'center',
})

export const Text = styled('div')({
  backgroundColor: '#222222',
  color: 'white',
  fontSize: '50px',
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  gap: '100px',
  verticalAlign: 'middle',
    textAlign: 'center',
})

export const Name = styled('div')({
  backgroundColor: '#222222',
  color: 'white',
  fontSize: '50px',
})

export const Choice = styled(Button)({
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'center',
  alignItems: 'center',
  maxWidth: '400px',
  width: '70vh',
  color: 'white',
  backgroundColor: '#999999',
  '&.Mui-selected, &.Mui-selected:hover': {
    color: 'white',
    backgroundColor: '#1976d2',
  },
})

export const NextQuestionButton = styled(Button)({
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'center',
  alignItems: 'center',
  textWrap: 'wrap',
  color: 'black',
  background: 'white',
  '&:hover': {
    background: 'white',
    color: 'purple',
  },
})

export const EndQuizButton = styled(Button)({
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    textWrap: 'wrap',
    color: 'black',
    background: 'white',
    '&:hover': {
      background: 'white',
      color: 'purple',
    },
  })
