import { Button, styled } from "@mui/material"

export const Container = styled("div")({
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    gap: '10px'
})

export const Home = styled(Button)({
})

export const Name = styled("div")({
    backgroundColor: '#222222',
    color: 'white',
    fontSize: '60px',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    gap: '100px',
    verticalAlign: 'middle',
      textAlign: 'center',
})


export const QuizPoints = styled("div")({
    backgroundColor: '#222222',
    color: 'white',
    fontSize: '60px',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    gap: '100px',
    verticalAlign: 'middle',
      textAlign: 'center',
})