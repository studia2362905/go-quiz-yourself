import { useLocation, useNavigate } from 'react-router-dom'
import { Container, Name, QuizPoints, Home } from './QuizEnd.styles'

const QuizEnd = () => {
  const navigate = useNavigate()
  const location = useLocation()
  const handleGoHome = () => {
    navigate(`/`)
  }

  return (
    <Container>
      <Name>Congratulations! </Name>
      <Name> Your Final Score Is: </Name>
      <QuizPoints>
        {location.state
          ? location.state.points && location.state.max
            ? location.state.points + '/' + location.state.max
            : 0
          : 0}{' '}
        Points!
      </QuizPoints>
      <Home variant='contained' onClick={() => handleGoHome()}>
        Home
      </Home>
    </Container>
  )
}

export { QuizEnd }
