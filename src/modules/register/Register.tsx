import { useState } from 'react'
import { IconButton, InputAdornment } from '@mui/material/'
import { Visibility, VisibilityOff } from '@mui/icons-material/'
import { useNavigate } from 'react-router-dom'
import { Button, Container, TextField } from './Register.styles'
import axios from 'axios'

const Register = () => {
  const navigate = useNavigate()
  const [showPassword, setShowPassword] = useState(false)
  const [login, setLogin] = useState<string>('')
  const [password, setPassword] = useState<string>('')
  const [error, setError] = useState<boolean>(false)

  const handleClickShowPassword = () => setShowPassword(!showPassword)
  const handleMouseDownPassword = () => setShowPassword(!showPassword)

  const handleRegister = () => {
    axios
    .post('/user/register', { name: login, password: password })
    .then(() => navigate(`/login`))
    .catch(function (error) {
      console.log(error)
      setLogin('')
      setPassword('')
      setError(true)
    })

  }
  return (
    <>
      <Container>
        <TextField
          fullWidth
          label='Login'
          variant='outlined'
          value={login}
          error={error}
          onChange={(e) => {
            setLogin(e.target.value)
            setError(false)
          }}
        />
        <TextField
          label={'Hasło'}
          fullWidth
          value={password}
          error={error}
          onChange={(e) => {
            setPassword(e.target.value)
            setError(false)
          }}
          type={showPassword ? 'text' : 'password'}
          InputProps={{
            endAdornment: (
              <InputAdornment position='end'>
                <IconButton
                  aria-label='toggle password visibility'
                  onClick={handleClickShowPassword}
                  onMouseDown={handleMouseDownPassword}
                >
                  {showPassword ? <Visibility /> : <VisibilityOff />}
                </IconButton>
              </InputAdornment>
            ),
          }}
        />

        <Button variant='contained' onClick={() => handleRegister()}>
          Register
        </Button>
      </Container>
    </>
  )
}
export { Register }
