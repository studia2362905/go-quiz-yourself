import { Button, styled } from '@mui/material'

export const Container = styled('div')({
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'center',
  alignItems: 'center',
})

export const NavBar = styled('div')({
  width: '100%',
  display: "flex",
  flexDirection: 'row',
  backgroundColor: '#222222',
  position: 'relative'
})

export const LoginButton = styled(Button)({
  color: 'white',
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  fontSize: '14px',
  gap: "100px",
})

export const AddQuizButton = styled(Button)({
  color: 'white',
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  fontSize: '14px',
})

export const Main = styled('div')({
  minHeight: '90vh',
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  border: ""
})

export const QuizList = styled('div')({
  display: 'flex',
  flexDirection: 'column',
  gap: '10px',
})

export const StartQuizButton = styled(Button)({
  textWrap: 'wrap',
  flex: 5,
  color: 'black',
  background: 'white',
  '&:hover': {
    background: 'white',
    color: 'purple',
  },
})

export const NoQuizesMessage = styled('div')({})

export const Title = styled('div')({
  color: 'white',
  fontSize: '28px',
  gap: '100px',
})
export const Points = styled('div')({
  color: 'white',
  fontSize: '28px',
})
export const StartQuizButtonContainter = styled('div')({
display: 'flex',
flexDirection: 'row',
minWidth: '90vw'
})
export const AcceptButton = styled(Button)({
  textWrap: 'wrap',
  flex: 1,
  color: 'black',
  background: 'white',
  '&:hover': {
    background: 'white',
    color: 'purple',
  },
})