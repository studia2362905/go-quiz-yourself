import { useNavigate } from 'react-router-dom'
import {
  AcceptButton,
  AddQuizButton,
  Container,
  LoginButton,
  Main,
  NavBar,
  NoQuizesMessage,
  Points,
  QuizList,
  StartQuizButton,
  StartQuizButtonContainter,
  Title,
} from './Home.styles'
import { useEffect, useState } from 'react'
import axios from 'axios'

const Home = () => {
  const navigate = useNavigate()
  const [quizes, setQuizes] = useState<
    {
      id: number
      name: string
      is_global: boolean
      _count: { _count: number }
    }[]
  >([])

    const [user, setUser] = useState<{id: number, name: string, points: number}>({id:0,name:"",points:0})

  useEffect(() => {
    const token = localStorage.getItem('access_token')
    const auth = {
      ...(token && { headers: { Authorization: `Bearer ${token}` } }),
    }
    axios
      .get('/quiz', auth)
      .then(function (response: {
        data: {
          id: number
          name: string
          is_global: boolean
          _count: { _count: number }
        }[]
      }) {
        setQuizes(response.data)
      })
      .catch(function (error) {
        console.log(error)
      })
    axios.get('/user/me', auth).then(function (response: {
      data: {
        id: number, name: string, points: number}}){setUser(response.data)}).catch(function (error) {
      console.log(error)
    })
  }, [])

  const handleEnterQuiz = (id: number) => {
    navigate(`/quiz/${id}`)
  }
  const handleAddQuiz = () => {
    navigate(`/quiz/add`)
  }
  const handleLogin = () => {
    navigate(`/login`)
  }
  const handleAcceptQuiz = (id: number) => {
    const token = localStorage.getItem('access_token')
    const auth = {
      ...(token && { headers: { Authorization: `Bearer ${token}` } }),
    }
    axios.patch(`/quiz/${id}`, auth).catch(function (error) {
      console.log(error)
    })
  }

  return (
    <>
      <Container>
        <NavBar>
          <Title sx={{ flexGrow: 3 }}>Go Quiz Yourself!</Title>
          <Points sx={{ flexGrow: 3 }}>
            Your Total Score Is: {user ? user.points + ' pkt' : ''}
          </Points>
          <LoginButton sx={{ flexGrow: 1 }} onClick={() => handleLogin()}>
            {user ? 'Log Out' : 'Log In'}
          </LoginButton>
          {user ? (
            <AddQuizButton sx={{ flexGrow: 1 }} onClick={() => handleAddQuiz()}>
              {' '}
              Add Quiz{' '}
            </AddQuizButton>
          ) : (
            ''
          )}
        </NavBar>
        <Main>
          <QuizList>
            {quizes && quizes.length > 0 ? (
              quizes.map((quiz) => {
                console.log(quiz)
                return (
                  <StartQuizButtonContainter key={quiz.id}>
                    <StartQuizButton
                      variant='contained'
                      onClick={() => handleEnterQuiz(quiz.id)}
                    >
                      {quiz.name}
                    </StartQuizButton>
                    {quiz.is_global === false ? (
                      <AcceptButton
                        variant='contained'
                        onClick={() => handleAcceptQuiz(quiz.id)}
                      >
                        {'Accept'}
                      </AcceptButton>
                    ) : (
                      ''
                    )}
                  </StartQuizButtonContainter>
                )
              })
            ) : (
              <NoQuizesMessage>No Quizes found</NoQuizesMessage>
            )}
          </QuizList>
        </Main>
      </Container>
    </>
  )
}

export { Home }
