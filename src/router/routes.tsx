import { Home, Login, AddQuiz, Quiz, QuizEnd, } from 'modules'
import { Register } from 'modules/register/Register'
import { Navigate } from 'react-router-dom'

export const routes = [
  {
    path: '/',
    element: <Home />,
  },
  {
    path: '/login',
    element: <Login />,
  },
  {
    path: '/register',
    element: <Register />,
  },
  {
    path: '/quiz',
    children: [
      {
        path: 'add',
        element: <AddQuiz />,
      },
      {
        path: ':quizID',
        element: <Quiz />,
      },
      {
        path: 'quizend',
        element: <QuizEnd />,
      },
      {
        index: true,
        element: <Navigate to='/' replace />,
      },
    ],
  },
]
