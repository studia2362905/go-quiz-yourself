import { routes as routesList } from './routes'
import { BrowserRouter, useRoutes } from "react-router-dom"

const RoutesList = () => {
  const routes = useRoutes(routesList)
  return routes
}

export const Router = () => {
  return (
    <BrowserRouter>
      <RoutesList />
    </BrowserRouter>
  )
}
