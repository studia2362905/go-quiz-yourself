import axios from "axios"

export const axiosConfig = () => {
    axios.defaults.baseURL = 'http://localhost:4000/api'
    axios.defaults.headers.common = {
      "Content-Type": "application/json"
    }
    axios.defaults.headers.post['Access-Control-Allow-Origin'] = '*'
}
