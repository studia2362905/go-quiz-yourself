export interface Choice {
    id: number
    question_id: number
    text: string
}

export interface Question {
    id: number,
    text: string,
    choices: Choice[]
}

export interface Quiz{
    id: number
    name: string
    questions: Question[] 
}