# Go Quiz Yourself

## What is the use of this repo 
Frontend for Web application to create and solve quizzes. You can use it without being logged in.

##### As User:
1. View and complete global quizes 
2. Add new quiz for admin to verify
3. Have fun

##### As System Admin:
1. All that user can do
2. Accept new quiz

## Prerequisites

### Install Node JS
Refer to https://nodejs.org/en/ to install nodejs

### Install create-react-app
Install create-react-app npm package globally. This will help to easily run the project and also build the source files easily. Use the following command to install create-react-app

```bash
npm install -g create-react-app
```

## Cloning and Running the Application in local

Clone the project into local

Install all the npm packages. Go into the project folder and type the following command to install all npm packages

```bash
npm install
```

In order to run the application Type the following command

```bash
npm start
```

The Application Runs on **localhost:3000**

## Used technologies and libraries

1. [React](https://reactjs.org) Library with [Typescript](https://www.typescriptlang.org)
2. [MUI](https://mui.com) for components
3. [Axios](https://axios-http.com) for API calls
